import DashboardLayout from '../layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../pages/NotFoundPage.vue'

// Admin pages

import EmpresaLista from 'src/pages/Empresa/EmpresaLista.vue'
import EmpresaForm from 'src/pages/Empresa/EmpresaForm.vue'
import FornecedorLista from 'src/pages/Fornecedor/FornecedorLista.vue'
import FornecedorForm from 'src/pages/Fornecedor/FornecedorForm.vue'


const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/overview'
  },
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/empresa',
    children: [

      {
        path: 'empresas',
        name: 'empresas',
        component: EmpresaLista
      },
      {
        path: 'empresa/novo',
        name: 'empresa-novo',
        component: EmpresaForm
      },
      {
        path: 'empresa/editar/:id',
        name: 'empresa-editar',
        component: EmpresaForm
      },
      {
        path: 'fornecedores',
        name: 'fornecedores',
        component: FornecedorLista
      },
      {
        path: 'fornecedor/novo',
        name: 'fornecedor-novo',
        component: FornecedorForm
      },
      {
        path: 'fornecedor/editar/:id',
        name: 'fornecedor-editar',
        component: FornecedorForm
      },

    ]
  },
  { path: '*', component: NotFound }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
