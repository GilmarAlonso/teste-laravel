<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Fornecedor;
use App\Models\Telefone;
use Illuminate\Http\Request;

class FornecedorController extends Controller
{
    public function index()
    {
        return response()->json(Fornecedor::all(), 200);
    }

    public function excluir($id)
    {
        $fornecedor = Fornecedor::find($id)->delete();
        if ($fornecedor) {
            return response()->json(true, 200);
        }
    }

    public function detalhes($id)
    {
        $fornecedor = Fornecedor::find($id);
        $fornecedor->empresa_id = Empresa::select('fantasia as label', 'id as code')->where('id', $fornecedor->empresa_id)->get();
        $telefones = Telefone::select('telefone')->where('fornecedor_id', $id)->get();
        $i = 1;
        $data = [];
        foreach ($telefones as $telefone) {
            $data[$i] = $telefone->telefone;
            ++$i;
        }
        $fornecedor->telefone = $data;

        return response()->json($fornecedor, 200);
    }

    public function inserir(Request $request)
    {
        $data = $request->all();
        $data['empresa_id'] = $data['empresa_id']['code'];
        $cpf_cnpj = HelperController::validaCnpj($data['cpf_cnpj']);
        if ($cpf_cnpj) {
            if (isset($data['id'])) {
                $fornecedor = Fornecedor::find($data['id']);
                $fornecedor->update($data);
            } else {
                $fornecedor = Fornecedor::create($data);
            }
            Telefone::where('fornecedor_id', $fornecedor->id)->delete();
            foreach ($data['telefone'] as $telefone) {
                Telefone::create(['fornecedor_id' => $fornecedor->id, 'telefone' => $telefone]);
            }

            return response()->json($fornecedor, 200);
        } else {
            $cpf_cnpj = HelperController::validaCPF($data['cpf_cnpj']);
            if ($cpf_cnpj) {
                $empresa = Empresa::find($data['empresa_id']);
                if ($empresa->uf == 'PR') {
                    if (isset($data['data_nascimento'])) {
                        $data_nascimento = explode('/', $data['data_nascimento']);
                        if (((int) date('Y') - (int) $data_nascimento[2]) >= 18) {
                            if (isset($data['id'])) {
                                $fornecedor = Fornecedor::find($data['id']);
                                $fornecedor->update($data);
                            } else {
                                $fornecedor = Fornecedor::create($data);
                            }
                            Telefone::where('fornecedor_id', $fornecedor->id)->delete();
                            foreach ($data['telefone'] as $telefone) {
                                Telefone::create(['fornecedor_id' => $fornecedor->id, 'telefone' => $telefone]);
                            }

                            return response()->json($fornecedor, 200);
                        } else {
                            return response()->json(['erro' => 'O fornecedor deve ter mais de 18 anos no paraná'], 200);
                        }
                    }
                } else {
                    if (isset($data['id'])) {
                        $fornecedor = Fornecedor::find($data['id']);
                        $fornecedor->update($data);
                    } else {
                        $fornecedor = Fornecedor::create($data);
                    }
                    Telefone::where('fornecedor_id', $fornecedor->id)->delete();
                    foreach ($data['telefone'] as $telefone) {
                        Telefone::create(['fornecedor_id' => $fornecedor->id, 'telefone' => $telefone]);
                    }

                    return response()->json($fornecedor, 200);
                }
            } else {
                return response()->json(['erro' => 'Por favor insira um cpf ou cnpj válido'], 200);
            }
        }
    }
}
