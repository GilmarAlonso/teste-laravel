<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    public function index()
    {
        return response()->json(Empresa::all(), 200);
    }

    public function getEmpresas()
    {
        $empresas = Empresa::select('fantasia as label', 'id as code')->get();

        return response()->json($empresas, 200);
    }

    public function excluir($id)
    {
        $empresa = Empresa::find($id)->delete();
        if ($empresa) {
            return response()->json(true, 200);
        }
    }

    public function detalhes($id)
    {
        $empresa = Empresa::find($id);

        return response()->json($empresa, 200);
    }

    public function inserir(Request $request)
    {
        $validated = $request->validate([
            'cnpj' => 'required|min:14',
            'fantasia' => 'required',
            'uf' => 'required',
        ]);
        $data = $request->all();

        $cnpj = HelperController::validaCnpj($data['cnpj']);
        if ($cnpj) {
            if (isset($data['id'])) {
                $empresa = Empresa::find($data['id'])->update($data);
            } else {
                $empresa = Empresa::create($validated);
            }

            return response()->json($empresa, 200);
        } else {
            return response()->json(['erro' => 'Por favor insira um cnpj válido'], 200);
        }
    }
}
