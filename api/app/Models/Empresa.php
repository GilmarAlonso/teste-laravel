<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;
    protected $fillable = [
        'uf',
        'cnpj',
        'fantasia',
    ];

    /**
     * Get the user associated with the Empresa.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa()
    {
        return $this->hasOne(Empresa::class, 'empresa_id', 'id');
    }
}
