<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('empresa/inserir', 'App\Http\Controllers\EmpresaController@inserir');
Route::get('/empresas-select', 'App\Http\Controllers\EmpresaController@getEmpresas');
Route::post('empresa/excluir/{id}', 'App\Http\Controllers\EmpresaController@excluir');
Route::get('empresas', 'App\Http\Controllers\EmpresaController@index');
Route::get('/empresa/detalhes/{id}', 'App\Http\Controllers\EmpresaController@detalhes');
Route::post('fornecedor/inserir', 'App\Http\Controllers\FornecedorController@inserir');
Route::post('fornecedor/excluir/{id}', 'App\Http\Controllers\FornecedorController@excluir');
Route::get('fornecedores', 'App\Http\Controllers\FornecedorController@index');
Route::get('/fornecedor/detalhes/{id}', 'App\Http\Controllers\FornecedorController@detalhes');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
